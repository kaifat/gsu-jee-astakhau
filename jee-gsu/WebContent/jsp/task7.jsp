<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>task 7</title>

<link rel="stylesheet" href="css/style.css">


</head>
<body>

<%@ include file="../blocks/menu.html"%>

<div class="main">
	<h1>Task #7</h1>	

<table class="container">

			<thead>
				<tr>
					<th><h1>Name</h1></th>
					<th><h1>Address</h1></th>
					<th><h1>Number</h1></th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="elem" items="${list1}" varStatus="status">
				<tr>
					<td><c:out value="${ elem.getName() }"></c:out> </td>
					<td><c:out value="${ elem.getAddress() }"></c:out> </td>
					<td><c:out value="${ elem.getNumber() }"></c:out> </td>
				</tr>
			</c:forEach>
			</tbody>
</table>


	<form name="task7Form" method="POST" action="controller">
		<input type="hidden" name="command" value="task7"/>
		<p align="center">Enter part of "Name" or "number" : 
		<input type="text" name="cond" value=""/>
		<input type="submit" value="submit"/></p>
	</form>	
	


<br/>
<table class="container">


			<tbody>
			<c:forEach var="elem" items="${list2}" varStatus="status">
				<tr>
					<td><c:out value="${ elem.getName() }"></c:out> </td>
					<td><c:out value="${ elem.getAddress() }"></c:out> </td>
					<td><c:out value="${ elem.getNumber() }"></c:out> </td>
				</tr>
			</c:forEach>
			</tbody>
</table>

</div>


</body>
</html>