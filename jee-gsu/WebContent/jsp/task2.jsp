<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>task 2</title>

<link rel="stylesheet" href="css/style.css">

<style>
	@import url(https://fonts.googleapis.com/css?family=Roboto:300);
	h3 {
	color : ${color};
	}

</style>
</head>
<body>

<%@ include file="../blocks/menu.html"%>

<div class="main">
	<h1>Task #2</h1>		
	<form name="task1Form" method="POST" action="controller">
		<input type="hidden" name="command" value="task2"/>
		Temperature:<br/>
		<input type="text" name="temperature" value=""/>
		<input type="submit" value="submit"/>
	</form>	
	<h3>Temperature = ${temperature}</h3>
	
</div>


</body>
</html>