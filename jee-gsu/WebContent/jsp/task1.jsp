<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>task 1</title>

<link rel="stylesheet" href="css/style.css">
</head>
<body>

<%@ include file="../blocks/menu.html"%>

<div class="main">
	<h1>Task #1</h1>		
	<h3>Country : <c:out value="${ country }"></c:out></h3>		
	<h3>Language : <c:out value="${ language }"></c:out></h3>		
	
</div>


</body>
</html>