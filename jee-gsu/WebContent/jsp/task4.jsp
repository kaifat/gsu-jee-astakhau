<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>task 4</title>

<link rel="stylesheet" href="css/style.css">


</head>
<body>

<%@ include file="../blocks/menu.html"%>

<div class="main">
	<h1>Task #4</h1>	
	<form name="task4Form" method="POST" action="controller">
		<input type="hidden" name="command" value="task4"/>
		angle:<br/>
		<input type="text" name="angle" value=""/>
		<select name="dropbox">
		  <option>cos</option>
		  <option>sin</option>
		  <option>tg</option>
		  <option>ctg</option>
		</select>
		<br/>
		
		<p><input name="radiobtn" type="radio" value="radians">Radians</p>
    	<p><input name="radiobtn" type="radio" value="celsius">Default</p>
		<input type="submit" value="submit"/>
	</form>	
	
	Type =    ${ type }
	<br/>
	Angle =   ${ angle }
	<br/>
	Radio =   ${ radio }
	<br/>
	Result =  ${ result }
 
</div>


</body>
</html>