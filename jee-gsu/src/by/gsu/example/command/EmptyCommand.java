package by.gsu.example.command;

import javax.servlet.http.HttpServletRequest;
import by.gsu.example.resource.ConfigurationManager;
public class EmptyCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		/* в случае ошибки или прямого обращения к контроллеру
		 * переадресация на главную страницу */
		String page = ConfigurationManager.getProperty("path.page.index");
		return page;
	}
}