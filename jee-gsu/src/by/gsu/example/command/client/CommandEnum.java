package by.gsu.example.command.client;

import by.gsu.example.command.*;

public enum CommandEnum {
	TASK1 {
		{
			this.command = new Task1Command();
		}
	},
	TASK2 {
		{
			this.command = new Task2Command();
		}
	},
	TASK3 {
		{
			this.command = new Task3Command();
		}
	},
	TASK4 {
		{
			this.command = new Task4Command();
		}
	},
	TASK5 {
		{
			this.command = new Task5Command();
		}
		
	},
	TASK6 {
		{
			this.command = new Task6Command();
		}
	},
	TASK7 {
		{
			this.command = new Task7Command();
		}
	};
			
	ActionCommand command;
	public ActionCommand getCurrentCommand() {
		return command;
	}
}
