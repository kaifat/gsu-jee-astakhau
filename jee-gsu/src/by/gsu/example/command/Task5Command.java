package by.gsu.example.command;

import javax.servlet.http.HttpServletRequest;

import by.gsu.example.resource.ConfigurationManager;

public class Task5Command implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			String type = request.getParameter("dropbox");
			switch (type) {
			case "car":
				request.setAttribute("img", "img/car.png");
				break;
			case "cat":
				request.setAttribute("img", "img/cat.jpg");
				break;
			case "nature":
				request.setAttribute("img", "img/nature.jpg");
				break;
			default:
				break;
			}	
		} catch (Exception e) {
		}
		
		
		page = ConfigurationManager.getProperty("path.page.task5");
		return page;
	}

}
