package by.gsu.example.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.example.model.Record;
import by.gsu.example.resource.ConfigurationManager;

public class Task7Command implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		List<Record> list1 = new ArrayList<>();
		list1.add(new Record("Vasya", "Sovetskay,12", "12345678"));
		list1.add(new Record("Petya", "Sovetskay,12", "87654321"));
		list1.add(new Record("Sergey", "Sovetskay,11", "12345678"));
		list1.add(new Record("Vlad", "Sovetskay,9", "87654321"));
		list1.add(new Record("Dima", "Sovetskay,1", "12345678"));
		list1.add(new Record("Natasha", "Sovetskay,2", "12345678"));
		
		request.setAttribute("list1", list1);
		
		
		try {
			String cond = request.getParameter("cond");
			//System.out.println(cond);
			
			List<Record> list2 = new ArrayList<>();
			for (Record elem : list1) {
				if (elem.getName().contains(cond) || elem.getNumber().contains(cond)) {
					list2.add(elem);
				}
//				if (cond.equals(elem.getName()) || cond.equals(elem.getNumber())) {
//					list2.add(elem);
//				}

			}
			request.setAttribute("list2", list2);
		} catch (Exception e) {
		}

		
		
		
		
		page = ConfigurationManager.getProperty("path.page.task7");
		return page;
	}

}
