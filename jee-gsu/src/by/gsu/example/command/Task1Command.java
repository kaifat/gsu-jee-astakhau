package by.gsu.example.command;


import javax.servlet.http.HttpServletRequest;

import by.gsu.example.resource.ConfigurationManager;

public class Task1Command implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String country = request.getLocale().getDisplayCountry();
		String language = request.getLocale().getDisplayLanguage();
		request.setAttribute("language", language);
		request.setAttribute("country", country);
		
		page = ConfigurationManager.getProperty("path.page.task1");
		return page;
	}

}
