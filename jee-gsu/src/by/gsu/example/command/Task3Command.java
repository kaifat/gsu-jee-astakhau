package by.gsu.example.command;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import by.gsu.example.resource.ConfigurationManager;

public class Task3Command implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		try {
			Date date = dateformat.parse("10/01/2017 10:45");
			request.setAttribute("date1", date);
			date = dateformat.parse("10/02/2017 21:35");
			request.setAttribute("date2", date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
//		request.setAttribute("date", date.toString());
		
		request.setAttribute("author", "Sergey Astakhau");
		
		page = ConfigurationManager.getProperty("path.page.task3");
		return page;
	}

}
