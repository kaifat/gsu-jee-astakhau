package by.gsu.example.command;

import javax.servlet.http.HttpServletRequest;

import by.gsu.example.resource.ConfigurationManager;

public class Task4Command implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		
		String type = request.getParameter("dropbox"); 
		String radio = request.getParameter("radiobtn");
		
		request.setAttribute("type", type );
		
		try {
			double angle = Double.parseDouble(request.getParameter("angle"));
			request.setAttribute("angle", angle);
			if (radio.equals("celsius")) {
				angle = Math.toRadians(angle);
			}
			double res = 0;
			switch (type) {
			case "cos":
				res = Math.cos(angle);
				break;
			case "sin":	
				res = Math.sin(angle);
				break;
			case "tg":	
				res = Math.tan(angle);
				break;
			case "ctg":	
				res = 1.0 / Math.tan(angle);
				break;
			default:
				break;
			}
			
			request.setAttribute("result", res);
			
			request.setAttribute("radio", radio);
		} catch (Exception e) {
		}
		
		
		page = ConfigurationManager.getProperty("path.page.task4");
		return page;
	}

}
