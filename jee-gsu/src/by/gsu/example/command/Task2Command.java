package by.gsu.example.command;

import javax.servlet.http.HttpServletRequest;

import by.gsu.example.resource.ConfigurationManager;

public class Task2Command implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String temp = request.getParameter("temperature");
		
		try {
			int temperature = Integer.parseInt(temp);
			String color = (temperature>0) ? "#FB667A": "#4DC3FA";
			
			request.setAttribute("temperature", temperature);
			request.setAttribute("color", color);
		} catch (NumberFormatException e) {
		}
		
		page = ConfigurationManager.getProperty("path.page.task2");
		return page;
	}

}
