package by.gsu.example.command;

import javax.servlet.http.HttpServletRequest;

import by.gsu.example.resource.ConfigurationManager;
import by.gsu.example.resource.Task6Manager;

public class Task6Command implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String temp = Task6Manager.getProperty("avg.temperature.string");
		String[] values = temp.split(";");
		int sum = 0;
		int buf = 0;
		for (String value : values) {
			try {
				buf =  Integer.parseInt(value);
			} catch (Exception e) {
			}
			sum = sum + buf;
			
		}
		sum = sum / values.length;
		request.setAttribute("temperature", sum);
		
		page = ConfigurationManager.getProperty("path.page.task6");
		return page;
	}

}
