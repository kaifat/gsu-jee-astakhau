package by.gsu.example.resource;

import java.util.ResourceBundle;
public class Task6Manager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.task6");
	// класс извлекает информацию из файла config.properties
	private Task6Manager() { }
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
